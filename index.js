const express = require("express");
const bodyParser = require("body-parser");
var nodemailer = require("nodemailer");


const app = express();
//enconded voor data van html elementen
app.use(bodyParser.urlencoded({extended:true}));
//  use all folders like img or css or static folders
app.use(express.static("public"));
app.use('/Images', express.static('Images'));
app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
   
});
app.post("/", (req, res) => {
   
    const fname = req.body.name;
    
    const email = req.body.email;
    const text = req.body.comments;
    console.log(fname);
    //res.send("Welkom "+ fname + " " + lname);
    // send email
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'yourEmail',
        pass: 'YourPassw'
      }
    });
    
    var mailOptions = {
      from: 'gaellouage@gmail.com',
      to: email,
      subject: 'Sending Email using Node.js',
      text: text
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        //terug naar root file als verzenden mail gelukt is
       res.redirect("/");
      }
    });
  
  });
app.listen(4000, () => {
    console.log("Server started on port 4000");
});